<?php
date_default_timezone_set("Asia/Jakarta");

  $SundayDate = fopen("sunday2015.txt", "w") or die("Cannot Open File/Dir");
  $timestamp = strtotime('01-01-2015');
  $timestampStop = strtotime('31-12-2015');

  while($timestamp <= $timestampStop)
  {
    $txt = date("d/m/Y",$timestamp);
    if(isSunday($timestamp))
      fwrite($SundayDate, $txt.PHP_EOL);

    $timestamp += 86400;
  }

  echo "done, all sunday date saved to sunday2015.txt";
  fclose($SundayDate);

  function isSunday($date) {
      $weekDay = date('w', $date);
      return ($weekDay == 0);
  }
?>
