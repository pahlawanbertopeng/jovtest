<?php
//Factory
class Pelajaran
{
    private $matapelajaran;
    private $guru;

    public function __construct($mp, $g)
    {
        $this->matapelajaran = $mp;
        $this->guru = $g;
    }

    public function get_make_and_model()
    {
        return $this->guru . ' mengajar ' . $this->matapelajaran;
    }
}

class AturPelajaran
{
    public static function assign($make, $model)
    {
        return new Pelajaran($make, $model);
    }
}

$smk1cmi = AturPelajaran::assign('Matematika', 'Pak Kumis');
echo "Factory Design : <br>";
echo $smk1cmi->get_make_and_model();

echo "<br>";

//Strategy
interface KeluaranInterface
{
    public function load($data);
}

class SAKeluaran implements KeluaranInterface
{
    public function load($data)
    {
        return serialize($data);
    }
}

class JsonKeluaran implements KeluaranInterface
{
    public function load($data)
    {
        return json_encode($data);
    }
}

class ArrayKeluaran implements KeluaranInterface
{
    public function load($data)
    {
        return $data;
    }
}

class DataPelajaran
{
    private $kel;

    public function setKel(KeluaranInterface $oKel)
    {
        $this->kel = $oKel;
    }

    public function loadKel($data)
    {
        return $this->kel->load($data);
    }
}

$pel = new DataPelajaran();

$data = array("buah1"=>"apel","buah2"=>"jambu","buah3"=>"stroberi");
echo "Strategi Design : <br>";
// keluaran array
$pel->setKel(new ArrayKeluaran());
$data = $pel->loadKel($data);
var_dump($data);
echo "<br>";
// keluaran JSON
$pel->setKel(new JsonKeluaran());
$data = $pel->loadKel($data);

var_dump($data);
