<?php
interface KeluaranInterface
{
    public function load($data);
}

class SAKeluaran implements KeluaranInterface
{
    public function load($data)
    {
        return serialize($data);
    }
}

class JsonKeluaran implements KeluaranInterface
{
    public function load($data)
    {
        return json_encode($data);
    }
}

class ArrayKeluaran implements KeluaranInterface
{
    public function load($data)
    {
        return $data;
    }
}

class DataPelajaran
{
    private $kel;

    public function setKel(KeluaranInterface $oKel)
    {
        $this->kel = $oKel;
    }

    public function loadKel($data)
    {
        return $this->kel->load($data);
    }
}

$pel = new DataPelajaran();

$data = array("buah1"=>"apel","buah2"=>"jambu","buah3"=>"stroberi");
echo "Strategi Design : <br>";
// keluaran array
$pel->setKel(new ArrayKeluaran());
$data = $pel->loadKel($data);
var_dump($data);
echo "<br>";
// keluaran JSON
$pel->setKel(new JsonKeluaran());
$data = $pel->loadKel($data);

var_dump($data);
