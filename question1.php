<?php
class Employee {
   private $salary;
   private $name;

   function __construct() {
       $this->salary = 0;
       $this->name = "No Name";
   }

   public function getSalary()
   {
      return $this->salary; 
   }

   public function getName()
   {
      return $this->name;
   }

   public function setSalary($salary)
   {
      $this->salary = $salary;
   }

   public function setName($name)
   {
      $this->name = $name;
   }
}
?>

<?php
$newEmployee = new Employee();

echo $newEmployee->getName() . "</br>";
$newEmployee->setName("Kopral Jono");
echo $newEmployee->getName() . "</br>";
?>
